# /*----------------------------------------------------------------------*\
# | Extensible multiplatform telescope mount controller                    |
# | Copyright (C) 2020 Atom Software Studios                               |
# |----------------------------------------------------------------------- |
# | This program is free software: you can redistribute it and/or modify   |
# | it under the terms of the GNU General Public License as published by   |
# | the Free Software Foundation, either version 3 of the License, or      |
# | (at your option) any later version.                                    |
# |                                                                        |
# | This program is distributed in the hope that it will be useful,        |
# | but WITHOUT ANY WARRANTY; without even the implied warranty of         |
# | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
# | GNU General Public License for more details.                           |
# |                                                                        |
# | You should have received a copy of the GNU General Public License      |
# | along with this program.  If not, see <https://www.gnu.org/licenses/>. |
# \*----------------------------------------------------------------------*/

# apt-get install gpsd-clients
# apt-get install libjson-c-dev
# apt-get install libsystemd-dev

CC=g++
CDEFS=-DPLATFORM=rpi4linux -DDIAG
CLIBS=-lm -lrt -lgps -ljson-c -lsystemd
CFLAGS=-Wall -O3 -Os -s $(CLIBS) $(CDEFS)
#CFLAGS=-Wall -O3 -Os -s $(CLIBS) $(CDEFS)
#CFLAGS=-Wall -O3 -g $(CLIBS) $(CDEFS)

.SILENT: astro system services devdrivers busdrivers lib clean

astro: busdrivers devdrivers lib services system
	echo "Compiling platform... "
	$(CC) ./astro-rpi4-linux.cpp -o ./astro ./obj/sys/* ./obj/lib/* ./obj/bus/* ./obj/devices/* ./obj/services/* $(CFLAGS)

system:
	echo "Compiling system... "
	mkdir -p ./obj/sys
	$(CC) -c ./src/sys/Sys.cpp -o ./obj/sys/Sys.o $(CFLAGS)

services:
	echo "Compiling services... "
	mkdir -p ./obj/services
	$(CC) -c ./src/services/ServiceAbstract/ServiceAbstract.cpp -o ./obj/services/ServiceAbstract.o $(CFLAGS)
	$(CC) -c ./src/services/Terminal/Terminal.cpp -o ./obj/services/Terminal.o $(CFLAGS)
	$(CC) -c ./src/services/InfoPrint/InfoPrint.cpp -o ./obj/services/InfoPrint.o $(CFLAGS)

devdrivers:
	echo "Compiling device drivers... "
	mkdir -p ./obj/devices
	$(CC) -c ./src/hardware/hal/Hal.cpp -o ./obj/devices/Hal.o $(CFLAGS)
	$(CC) -c ./src/hardware/devices/LinuxTimer/LinuxTimer.cpp -o ./obj/devices/LinuxTimer.o $(CFLAGS)
	$(CC) -c ./src/hardware/devices/Gpsd/Gpsd.cpp -o ./obj/devices/Gpsd.o $(CFLAGS)
	$(CC) -c ./src/hardware/devices/TMC5160/TMC5160SPI.cpp -o ./obj/devices/TMC5160SPI.o $(CFLAGS)
	$(CC) -c ./src/hardware/devices/LinuxJoystick/LinuxJoystick.cpp -o ./obj/devices/LinuxJoystick.o $(CFLAGS)

busdrivers:
	echo "Compiling bus drivers... "
	mkdir -p ./obj/bus
	$(CC) -c ./src/hardware/bus/gpio/BCM2708Gpio/BCM2708Gpio.cpp -o ./obj/bus/BCM2708Gpio.o $(CFLAGS)
	$(CC) -c ./src/hardware/bus/spi/SpiDev/SpiDev.cpp -o ./obj/bus/SpiDev.o $(CFLAGS)

lib:
	echo "Compiling libraries... "
	mkdir -p ./obj/lib
	$(CC) -c ./src/lib/Diag/Diag.cpp -o ./obj/lib/Diag.o $(CFLAGS)
	$(CC) -c ./src/lib/Diag/FileLogger/FileLogger.cpp -o ./obj/lib/FileLogger.o $(CFLAGS)
	$(CC) -c ./src/lib/Diag/JournalLogger/JournalLogger.cpp -o ./obj/lib/JournalLogger.o $(CFLAGS)
	$(CC) -c ./src/lib/AstroConfig/AstroConfig.cpp -o ./obj/lib/AstroConfig.o $(CFLAGS)

clean:
	rm -rf ./obj/*
	rm -f ./astro
