/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef platform_rpi4_hpp
#define platform_rpi4_hpp

#define SPIDEV_DEVICE                "/dev/spidev0.0"
#define SPIDEV_BITSPERWORD           8
#define SPIDEV_MODE                  3
#define SPIDEV_SPEED                 1000000L

#define GPSD_HOSTNAME                "localhost"
#define GPSD_PORT                    "2947"

#define MOTION_CONTROLLER_DEC_CS_PIN 10
#define MOTION_CONTROLLER_RA_CS_PIN  11
#define MOTION_CONTROLLER_FOC_CS_PIN 12

#define LINUX_JOYSTICK_DEVICE        "/dev/input/js0"

/* ------------------------------------------------------- */
/* Configure buses                                         */
/* ------------------------------------------------------- */

// --------------------- GPIO BUS ------------------------

/*
#include "../../../hardware/bus/gpio/BCM2708Gpio/BCM2708Gpio.hpp"

class BCM2708Gpio* busGpio0 = new BCM2708Gpio();
*/

#include "../../../hardware/bus/gpio/Gpio.hpp"

class Gpio* busGpio0 = nullptr;

// --------------------- SPI BUS -------------------------

/*
#include "../../../hardware/bus/spi/SpiDev/SpiDev.hpp"

struct SpiDevConfig busSpi0Config = {
   .device = (char*)SPIDEV_DEVICE,
   .bitsPerWord = SPIDEV_BITSPERWORD,
   .mode = SPIDEV_MODE,
   .speed = SPIDEV_SPEED
};

class SpiDev* busSpi0 = new SpiDev(&busSpi0Config);
*/

#include "../../../hardware/bus/spi/Spi.hpp"

class Spi* busSpi0 = nullptr;

// --------------------- I2C BUS -------------------------


/* ------------------------------------------------------- */
/* Configure devices                                       */
/* ------------------------------------------------------- */

// ----------------- Motion Controllers ------------------

/** Motion controller: TMC5160SPI driver */

#include "../../../hardware/devices/TMC5160/TMC5160SPI.hpp"

struct TMC5160SPIConfig devMCDecConfig = {
   .spi = busSpi0,
   .gpio = busGpio0,
   .mode = Position,
   .csPin = MOTION_CONTROLLER_DEC_CS_PIN,
};

struct TMC5160SPIConfig devMCRaConfig = {
   .spi = busSpi0,
   .gpio = busGpio0,
   .mode = Position,
   .csPin = MOTION_CONTROLLER_RA_CS_PIN
};

struct TMC5160SPIConfig devMCFocConfig = {
   .spi = busSpi0,
   .gpio = busGpio0,
   .mode = Position,
   .csPin = MOTION_CONTROLLER_FOC_CS_PIN
};

class TMC5160SPI* devMCDec = new TMC5160SPI(&devMCDecConfig);
class TMC5160SPI* devMCRa = new TMC5160SPI(&devMCRaConfig);
class TMC5160SPI* devMCFoc = new TMC5160SPI(&devMCFocConfig);

// ------------ Localization devices (GPS) ---------------

/** Localization: GPSD driver */

/*
#include "../../../hardware/devices/LinuxTimer/LinuxTimer.hpp"

class LinuxTimer* devGpsdTimer = new LinuxTimer();

#include "../../../hardware/devices/Gpsd/Gpsd.hpp"

struct GpsdConfig devGpsdConfig = {
   .timer = devGpsdTimer,
   .hostname = (char*)GPSD_HOSTNAME,
   .port = (char*)GPSD_PORT
};

class Gpsd* devGpsd = new Gpsd(&devGpsdConfig);
*/


// ------------------ Controllers ------------------------

/** Linux joystick */

/*
#include "../../../hardware/devices/LinuxJoystick/LinuxJoystick.hpp"

class LinuxJoystick* devLinuxJoystick = new LinuxJoystick((char*)LINUX_JOYSTICK_DEVICE);
*/

// -------------------------------------------------------

/** !!! MAKE SURE THE LAST DEVICE IS 'nullptr' IN THE FOLLOWING ARRAY !!! **/

class Hal* drivers[] = {
   //devGpsdTimer,
   //devGpsd,
   devMCDec,
   devMCRa,
   devMCFoc,
   //devLinuxJoystick,
   nullptr
};

#endif

