/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef diagnostics_hpp
#define diagnostics_hpp

#include "../../../lib/Diag/Diag.hpp"

/*
#include "../../../lib/Diag/SerialLogger/SerialLogger.hpp"
*/

#include "../../../lib/Diag/FileLogger/FileLogger.hpp"
class FileLogger* fileLogger0 = new FileLogger();    // Screen logger
class FileLogger* fileLogger1 = new FileLogger();    // File logger

#include "../../../lib/Diag/JournalLogger/JournalLogger.hpp"
class JournalLogger* journalLogger = new JournalLogger();

class Logger* loggers[] = {
   fileLogger0,
   fileLogger1,
   journalLogger,
   nullptr
};

class Diag* diagnostics = new Diag(loggers);

#endif