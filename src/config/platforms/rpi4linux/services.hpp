/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef services_linux_hpp
#define services_linux_hpp

#include "drivers.hpp"

/* ------------------------------------------------------- */
/* Setup services                                          */
/* ------------------------------------------------------- */

/**
 * Terminal service is used by InfoPrint service to print
 * formated driver and service information to the system
 * terminal. Additionally, it is used to print diagnostic
 * logs by Diagnostics library
 */
#include "../../../services/Terminal/Terminal.hpp"
class Terminal* terminal = new Terminal(&systemContext);

/**
 * InfoPrint collects states and the data form drivers and
 * services and prints them formated to the terminal.
 * It should not be included in the release build as it
 * slows down the system loop dramatically.
 * If new drivers and services are developed the InfoPrint
 * class should be extended to support these new components.
 */
#include "../../../services/InfoPrint/InfoPrint.hpp"
class InfoPrint* serviceInfoPrint = new InfoPrint(&systemContext, terminal);

class Service* services[] = {
   terminal,
   serviceInfoPrint,
   nullptr
};

#endif