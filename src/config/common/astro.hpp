/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef astro_hpp
#define astro_hpp

// Sidereal day time in miliseconds
#define SISDEREAL_TIME_MS 86164091

// RA AXIS - Microsteps per revolution
// Worm ratio * Full steps * Microsteps per step * Planetary ratio
// 144 * 200 * 256 * 5
#define RA_USTEPS_REV 36864000

// RA AXIS - Microsteps per second (sidereal day)
#define RA_SIDEREAL_STEPS_S (1000 * (SISDEREAL_TIME_MS / RA_USTEPS_REV))

// HA AXIS - Microsteps per revolution
// Worm ratio * Full steps * Microsteps per step
// 144 * 200 * 256
#define FOC_USTEPS_REV 7372800

#endif