/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef Config_h
#define Config_h

/* ------------------------------------------------------------------- */
/* Helper macros                                                       */
/* ------------------------------------------------------------------- */

#define STR(arg) #arg         // Converts argument to string
#define XSTR(arg) STR(arg)    // Expands arg and converts it to string

/* ------------------------------------------------------------------- */
/* Common configs, definitions and declarations                        */
/* ------------------------------------------------------------------- */

#include "common/options.hpp"
#include "common/astro.hpp"

// Service context is initialized in entry function of the program
// but must be allocated before drivers and services are initialized

#include "../sys/Sys.hpp"
struct SysContext systemContext;

/* ------------------------------------------------------------------- */
/* Platform configuration                                              */
/* ------------------------------------------------------------------- */

#define PLATFORM_DRIVERS XSTR(platforms/PLATFORM/drivers.hpp)
#include PLATFORM_DRIVERS

/* ------------------------------------------------------------------- */
/* Services configuration                                              */
/* ------------------------------------------------------------------- */

#define PLATFORM_SERVICES XSTR(platforms/PLATFORM/services.hpp)
#include PLATFORM_SERVICES

/* ------------------------------------------------------------------- */
/* Diagnostics configuration                                           */
/* ------------------------------------------------------------------- */

#ifdef DIAG
#define PLATFORM_DIAGNOSTICS XSTR(platforms/PLATFORM/diagnostics.hpp)
#include PLATFORM_DIAGNOSTICS
#endif

/* ------------------------------------------------------------------- */
/* System instantiation                                                */
/* ------------------------------------------------------------------- */

class Sys sys = Sys(&systemContext);

#endif
