/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#include "Sys.hpp"

#include <unistd.h>

Sys::Sys(SysContext* systemContext) {

   this->systemContext = systemContext;
   this->finalized = false;

}


Sys::~Sys() {

   if (!this->initialized) return;
   if (this->running) this->stop();
   if (!this->finalized) this->_finalize();

}


bool Sys::initialize() {

   if (this->initialized || this->finalized) return false;

   if (!initializeDevices()) return false;
   if (!initializeServices()) return false;

   this->initialized = true;

   return true;

}


void Sys::run() {

   if (!this->initialized || this->running) return;

   this->running = true;
   this->looping = true;

   this->onLoopBegin();

   while (this->looping) {
      this->loop();
   }

   this->onLoopEnd();

   this->running = false;

}


void Sys::stop() {
   if (!this->initialized || !this->running) return;
   this->looping = false;
}


void Sys::finalize() {

   if (!this->initialized) return;
   if (this->running) this->stop();
   if (!this->finalized) this->_finalize();

}

/* -------------------------------------------------------- */
/* Protected                                                */
/* -------------------------------------------------------- */


/**
 * Configured devices initialization
 */
bool Sys::initializeDevices() {

   this->onDevicesInitBegin();

   int i = 0;

   while (this->systemContext->drivers[i] != nullptr) {

      this->onDeviceInitBegin(this->systemContext->drivers[i]);

      if (!this->systemContext->drivers[i]->initialize()) {
         this->onDeviceInitFail(this->systemContext->drivers[i]);
         return false;
      }

      this->onDeviceInitDone(this->systemContext->drivers[i]);
      i++;

   }

   this->onDevicesInitDone();

   return true;

}


/**
 * Configured services initialization
 */
bool Sys::initializeServices() {

   this->onServicesInitBegin();

   int i = 0;

   while (this->systemContext->services[i] != nullptr) {

      this->onServiceInitBegin(this->systemContext->services[i]);

      if (!this->systemContext->services[i]->initialize()) {
         this->onServiceInitFail(this->systemContext->services[i]);
         return false;
      }

      this->onServiceInitDone(this->systemContext->services[i]);

      i++;

   }

   this->onServicesInitDone();

   return true;

}

/**
 * Main application loop
 * Can be interrupted externally by setting the looping property to false
 */
void Sys::loop() {

   int i = 0;
   while (this->systemContext->services[i] != nullptr) {
      this->onLoopOnServiceUpdate(this->systemContext->services[i]);
      this->systemContext->services[i]->update();
      i++;
      usleep(100);
   }

   this->systemContext->loopCounter++;

}


void Sys::_finalize() {

   this->finalizeServices();
   this->finalizeDevices();
   this->finalized = true;

}


/**
 * Configured devices initialization
 */
void Sys::finalizeDevices() {

   this->onDevicesFinalizeBegin();

   // count drivers
   int j = 0;
   while (this->systemContext->drivers[j] != nullptr) j++;

   // finalize drivers in reverse order
   for (int i = j - 1; i >= 0; i--) {
      this->onDeviceFinalize(this->systemContext->drivers[i]);
      this->systemContext->drivers[i]->finalize();
   }

   this->onDevicesFinalizeDone();

}


/**
 * Configured services initialization
 */
void Sys::finalizeServices() {

   this->onServicesFinalizeBegin();

   // count services
   int j = 0;
   while (this->systemContext->services[j] != nullptr) j++;

   // finalize services in reverse order
   for (int i = j - 1; i >= 0; i--) {
      this->onServiceFinalize(this->systemContext->services[i]);
      this->systemContext->services[i]->finalize();
   }

   this->onServicesFinalizeDone();

}

void Sys::onDevicesInitBegin() {
   #ifdef DIAG
   systemContext->diag->info((char*)"Sys::initializeDevices", (char*)"Initializing devices");
   #endif
};

void Sys::onDeviceInitBegin(Hal* device) {
   #ifdef DIAG
   systemContext->diag->debug((char*)"Sys::initializeDevices", (char*)"Initializing device '%s'", device->getDeviceName());
   #endif
};

void Sys::onDeviceInitDone(Hal* device) {
   #ifdef DIAG
   systemContext->diag->debug((char*)"Sys::initializeDevices", (char*)"Device '%s' initialized succesfully", device->getDeviceName());
   #endif
};

void Sys::onDeviceInitFail(Hal* device) {
   #ifdef DIAG
   systemContext->diag->error((char*)"Sys::initializeDevices", (char*)"Device '%s' failed to initialize with state '%s'", device->getDeviceName(), device->getState());
   #endif
};

void Sys::onDevicesInitDone() {
   #ifdef DIAG
   systemContext->diag->info((char*)"Sys::initializeDevices", (char*)"Initializing devices finished sucessfully");
   #endif
};

void Sys::onServicesInitBegin() {
   #ifdef DIAG
   systemContext->diag->info((char*)"Sys::initializeServices", (char*)"Initializing services");
   #endif
};

void Sys::onServiceInitBegin(Service* service) {
   #ifdef DIAG
   systemContext->diag->debug((char*)"Sys::initializeServices", (char*)"Initializing service '%s'", service->getName());
   #endif
};

void Sys::onServiceInitDone(Service* service) {
   #ifdef DIAG
   systemContext->diag->debug((char*)"Sys::initializeServices", (char*)"Service '%s' initialized succesfully (state = %s)", service->getName(), service->getState());
   #endif
};

void Sys::onServiceInitFail(Service* service) {
   #ifdef DIAG
   systemContext->diag->error((char*)"Sys::initializeServices", (char*)"Service '%s' failed to initialize with state '%s'", service->getName(), service->getState());
   #endif
};

void Sys::onServicesInitDone() {
   #ifdef DIAG
   systemContext->diag->info((char*)"Sys::initializeServices", (char*)"Initializing services finished successfully");
   #endif
};

void Sys::onLoopBegin() {
   #ifdef DIAG
   systemContext->diag->info((char*)"Sys::loop", (char*)"Starting system loop");
   #endif
};

void Sys::onLoopOnServiceUpdate(Service* service) {
   #ifdef DIAG
   // systemContext->diag->debug((char*)"Sys::loop", (char*)"Updating service %s", service->getName());
   #endif
};

void Sys::onLoopEnd() {
   #ifdef DIAG
   systemContext->diag->info((char*)"Sys::loop", (char*)"System loop stopped");
   #endif
};

void Sys::onDevicesFinalizeBegin() {
   #ifdef DIAG
   systemContext->diag->info((char*)"Sys::finalizeDevices", (char*)"Finalizing devices");
   #endif
}

void Sys::onDeviceFinalize(Hal* device) {
   #ifdef DIAG
   systemContext->diag->debug((char*)"Sys::finalizeDevices", (char*)"Finalizing device '%s'", device->getDeviceName());
   #endif

}

void Sys::onDevicesFinalizeDone() {
   #ifdef DIAG
   systemContext->diag->info((char*)"Sys::finalizeDevices", (char*)"Devices fnalized");
   #endif
}

void Sys::onServicesFinalizeBegin() {
  #ifdef DIAG
   systemContext->diag->info((char*)"Sys::finalizeDevices", (char*)"Finalizing services");
   #endif

}

void Sys::onServiceFinalize(Service* service) {
   #ifdef DIAG
   systemContext->diag->debug((char*)"Sys::finalizeServices", (char*)"Finalizing service '%s'", service->getName());
   #endif
}

void Sys::onServicesFinalizeDone() {
  #ifdef DIAG
   systemContext->diag->info((char*)"Sys::finalizeServices", (char*)"Services finalized");
   #endif

}
