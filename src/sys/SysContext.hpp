/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef SysContext_hpp
#define SysContext_hpp

#include "../hardware/hal/Hal.hpp"
#include "../services/Service.hpp"

#ifdef DIAG
#include "../lib/Diag/Diag.hpp"
#endif

struct SysContext {

   // Statically defined or dynamically created options
   // depend on platform (i.e. arduino = hardcoded or eeprom, linux = cmd line args + config file)
   struct Options* options;

   // Nullptr terminated list of devices
   struct Hal** drivers;

   // Nullptr terminated list of services
   struct Service** services;

   // Holds number of loops executed since the system started
   unsigned long long loopCounter;

   #ifdef DIAG
   // Holds a reference to the diagnostics object
   class Diag* diag;
   #endif

};

#endif