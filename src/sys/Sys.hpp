/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef App_hpp
#define App_hpp

#include "SysContext.hpp"

class Sys {

   public:

      Sys(SysContext* systemContext);

      virtual ~Sys();

      virtual bool initialize();
      virtual void run();
      virtual void stop();
      virtual void finalize();
      virtual void loop();

   private:

      struct SysContext* systemContext;

      bool initialized;
      bool finalized;
      bool running;
      bool looping;

      virtual bool initializeDevices();
      virtual bool initializeServices();
      virtual void _finalize();
      virtual void finalizeServices();
      virtual void finalizeDevices();

      virtual void onDevicesInitBegin();
      virtual void onDeviceInitBegin(Hal* device);
      virtual void onDeviceInitDone(Hal* device);
      virtual void onDeviceInitFail(Hal* device);
      virtual void onDevicesInitDone();

      virtual void onServicesInitBegin();
      virtual void onServiceInitBegin(Service* service);
      virtual void onServiceInitDone(Service* service);
      virtual void onServiceInitFail(Service* service);
      virtual void onServicesInitDone();

      virtual void onLoopBegin();
      virtual void onLoopOnServiceUpdate(Service* service);
      virtual void onLoopEnd();

      virtual void onDevicesFinalizeBegin();
      virtual void onDeviceFinalize(Hal* device);
      virtual void onDevicesFinalizeDone();

      virtual void onServicesFinalizeBegin();
      virtual void onServiceFinalize(Service* service);
      virtual void onServicesFinalizeDone();

};

#endif
