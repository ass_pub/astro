/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#include "StepperEQMount.hpp"

/*
StepperEQMount::StepperEQMount(Stepper *dec, Stepper *ra) {

   this->state = (char *)"Not initialized";

   this->dec = dec;
   this->ra = ra;

}
*/

char *StepperEQMount::getState() {
   return this->state;
}

char *StepperEQMount::getDecMotorState() {
   //return this->dec->getState();
}

char *StepperEQMount::getRaMotorState() {
//   return this->ra->getState();
}

bool StepperEQMount::init() {
/*
   if (!this->dec->init()) {
      this->state = (char *)"Failed to initialize DEC motor";
      return false;
   }

   if (!this->ra->init()) {
      this->state = (char *)"Failed to initialize RA motor";
      return false;
   }
*/
   this->state = (char *)"Ready";

   return true;

}