/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef ServiceAbstract_hpp
#define ServiceAbstract_hpp

#include "../Service.hpp"

class ServiceAbstract: public Service {

   protected:
      // Holds pointer to the system context structure
      struct SysContext* systemContext;

      // holds the pointer to buffer used to construct the state message
      char* stateBuffer;

      // holds the pointer to the latest state message
      char* state;

      // holds information if the initialize was called
      bool initialized;

      // holds information if the service is enabled and can be called in loop
      bool enabled;

   public:
      ServiceAbstract(SysContext* systemContext);
      virtual ~ServiceAbstract();

      virtual const char* getState();
      virtual bool getEnabled();

};

#endif