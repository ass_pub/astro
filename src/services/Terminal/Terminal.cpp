/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef ARDUINO

#include "Terminal.hpp"

// initialize Terminal static members
class Terminal* Terminal::terminal = nullptr;


/**
 * Construct the terminal object
 */
Terminal::Terminal(SysContext* systemContext): ServiceAbstract(systemContext) {

   this->termiosOrig = nullptr;
   this->cursorHidden = false;
   this->resizing = false;
   this->buffered = false;
   this->bufferw = 0;
   this->bufferh = 0;
   this->buffer = nullptr;
   this->bufferChanged = false;
   this->x = 0;
   this->y = 0;

};


/**
 * Configures a single configurable property (only enabled can be configured for the terminal)
 */
bool Terminal::configure(char* key, const void* value) {

   if (strcmp(key, (char*)"enabled") == 0 ) {

      if (!*(bool*)value && this->enabled) {
         this->state = (char*)"Terminal can't be disabled when it was enabled previously";
         return false;
      }

      this->enabled = *(bool*)value;
      return true;

   }

   return false;

}


// Does not apply for Terminal, but must be implemented
bool Terminal::reconfigure() {
   return true;
}


/**
 * Initializes the *nix terminal
 */
bool Terminal::initialize() {

   if (!this->enabled) {
      this->state = (char*)"Service is disabled";
      return true;
   }
   // structure to store the current terminal state
   struct termios orig;

   #ifdef DIAG
   this->systemContext->diag->debug((char*)"Terminal::initialize", (char*)"Initializing terminal service");
   #endif

   // check if another instance is running
   if (this->terminal != nullptr) {
      this->state = (char*)"Single instance of the trerminal is allowed only";
      return false;
   }

   // store pointer to terminal object to be possible to handle resize events using member function
   this->terminal = this;

   // get the current terminal state
   if (tcgetattr(STDIN_FILENO, &orig) == -1) {
      this->state = (char*)"Failed to obtain the terminal state";
      return false;
   }

   // allocate memory for the termios structure
   this->termiosOrig = (termios*)malloc(sizeof(termios));
   if (this->termiosOrig == nullptr) {
      this->state = (char*)"Failed to allocate memory for termios structure";
      return false;
   }

   // store current terminal state in the allocated memory
   memcpy(this->termiosOrig, &orig, sizeof(termios));

   // register window resize signal handler
   if (signal(SIGWINCH, this->onSIGWINCH) == SIG_ERR) {
      this->state = (char*)"Failed to register winow resize signal handler";
      return false;
   };

   this->initialized = true;
   this->state = (char*)"Ready";

   return true;

}

/**
 * Resets the terminal to the state
 */
void Terminal::finalize() {

   #ifdef DIAG
   this->systemContext->diag->debug((char*)"Terminal::finalize", (char*)"Finalizing terminal service");
   #endif

   // finish alternate mode
   if (this->buffered) {
      this->doneAlternate();
   }

   // show the cursor
   if (this->cursorHidden) this->showCursor();

   // free the buffer if it is allocated,
   if (this->buffer != nullptr) {
      free(this->buffer);
      this->buffer = nullptr;
   }

   // restore original termios state
   if (this->termiosOrig != nullptr) {
      tcgetattr(STDIN_FILENO, this->termiosOrig);
      free(this->termiosOrig);
      this->termiosOrig = nullptr;
   }

   // release window size change signal hanlder
   signal(SIGWINCH, SIG_DFL);

   this->initialized = false;

}

/**
 * In buffered mode the buffer is printed out and cleared
 */
void Terminal::update() {

   if (!this->initialized || !this->buffered || !this->bufferChanged) return;

   if (this->resizing) {
      this->resize();
      return;
   }

   if (this->buffer == nullptr) return;

   // move to left top corner
   printf("\x1b[1;1H");

   // print out the whole buffer
   printf(this->buffer);

   // clear buffer and prepare it for further rendering
   this->clear();

   // flush the contents of the stdout buffer
   fflush(stdout);

}

/**
 * Disables writing output charecters caused by user keypresses
 */
void Terminal::inputEchoOff() {

   if (!this->initialized || this->termiosOrig == nullptr) return;

   #ifdef DIAG
   this->systemContext->diag->debug((char*)"Terminal::inputEchoOff", (char*)"Switching keyboard echo off");
   #endif

   struct termios t;
   memcpy(&t, this->termiosOrig, sizeof(termios));;

   t.c_lflag &= ~(ECHO);
   tcsetattr(STDIN_FILENO, TCSAFLUSH, &t);

}

/**
 * Resets the terminal to its original state
 */
void Terminal::inputReset() {

   if (!this->initialized || this->termiosOrig == nullptr) return;

   #ifdef DIAG
   this->systemContext->diag->debug((char*)"Terminal::inputReset", (char*)"Reseting termios original state");
   #endif

   tcsetattr(STDIN_FILENO, TCSAFLUSH, this->termiosOrig);

}

/**
 * Starts aternate terminal and enters the full screen mode
 */
void Terminal::startAlternate() {

   if (!this->initialized || this->buffered) return;

   #ifdef DIAG
   this->systemContext->diag->debug((char*)"Terminal::startAlternate", (char*)"Starting alternate terminal");
   #endif

   this->buffered = true;

   this->resize();
   this->x = 0;
   this->y = 0;

   printf("\x1b[?1049h\x1b[2J\x1b[1;1H");
   fflush(stdout);
}

/**
 * Exits the alternate terminal and full screen mode
 */
void Terminal::doneAlternate() {

   if (!this->initialized || !this->buffered) return;

   #ifdef DIAG
   this->systemContext->diag->debug((char*)"Terminal::doneAlternate", (char*)"Exiting alternate terminal");
   #endif

   this->buffered = false;

   printf("\x1b[?1049l");
   fflush(stdout);

}

/**
 * Hides the cursor
 */
void Terminal::hideCursor() {

   if (!this->initialized) return;

   printf("\x1b[?25l");
   this->cursorHidden = true;

}


/**
 * Shows the cursor
 */
void Terminal::showCursor() {

   if (!this->initialized) return;

   printf("\x1b[?25h");
   this->cursorHidden = false;

}

/**
 * Clears the terminal or the back buffer in full-screen mode
 */
void Terminal::clear() {

   if (!this->initialized) return;

   if (!this->buffered) {
      printf("\x1b[2J");
      this->moveTo(0, 0);
      return;
   }

   if (!this->initialized || this->buffer == nullptr) return;

   memset(this->buffer, ' ', this->bufferw * this->bufferh);
   this->buffer[this->bufferw * this->bufferh] = 0;
   this->bufferChanged = true;
   this->moveTo(0, 0);

}

/**
 * Moves a cursor to requested position (coorinates starts at 0, 0)
 */
void Terminal::moveTo(int x, int y) {

   if (!this->initialized) return;

   if (!this->buffered) {
      printf("\x1b[%d;%dH", y + 1, x + 1);
      return;
   } else {
      if (x >= 0 && x < this->bufferw) this->x = x;
      if (y >= 0 && y < this->bufferh) this->y = y;
   }

}

/**
 * Prints a text to the terminal to the current cursor position
 */
void Terminal::print(const char *text, ...) {

   if (!this->initialized) return;

   va_list args;
   va_start(args, text);

   if (!this->buffered) {
      vprintf(text, args);
   } else {
      this->printout(text, args);
   }

   va_end(args);

}

/**
 * Prints a text to the terminal to the current cursor position
 */
void Terminal::print(const char *text, va_list args) {

   if (!this->initialized) return;

   if (!this->buffered) {
      vprintf(text, args);
   } else {
      this->printout(text, args);
   }

}

/**
 * Prints a text to the terminal to the requested position
 */
void Terminal::printAt(int x, int y, const char *text, ...) {

   if (!this->initialized) return;

   this->moveTo(x, y);

   va_list args;
   va_start(args, text);
   this->print(text, args);
   va_end(args);

}

/**
 * Prints a text to the terminal to the requested position
 */
void Terminal::printAt(int x, int y, const char *text, va_list args) {

   if (!this->initialized) return;

   this->moveTo(x, y);
   this->print(text, args);

}

/**
 * Flushes the stdout buffer (in non-full screen mode only)
 */
void Terminal::flush() {

   if (!this->initialized) return;
   fflush(stdout);

}

/**
 * Handles the terminal resize event and set's the flag
 * the buffer must be resized if in full screen mode
 */
void Terminal::onResize() {

   if (!this->initialized) return;

   if (!this->resizing) this->resizing = true;

}

/**
 * Rezized the buffer according to the current terminal window size
 */
void Terminal::resize() {

   if (!this->buffered) return;

   struct winsize ws;
   ioctl(STDOUT_FILENO, TIOCGWINSZ, &ws);

   // if old buffer exist, release it
   if (this->buffer != nullptr) {
      free(this->buffer);
      this->buffer = nullptr;
   }

   this->bufferw = ws.ws_col;
   this->bufferh = ws.ws_row;
   this->bufferChanged = true;

   // allocate buffer according to window size
   int bufsize = ws.ws_row * ws.ws_col + 1;

   if (bufsize == 0) return;

   char* buf = (char*)malloc(bufsize);

   // clean the buffer
   memset(buf, ' ', bufsize);
   buf[bufsize - 1] = 0;


   // store buffer info
   this->buffer = buf;

   if (this->cursorHidden) this->hideCursor();

   this->resizing = false;

}

/**
 * Used internally to print output to the back buffer
 */
void Terminal::printout(const char *text, va_list args) {

   if (this->buffer == nullptr) return;

   char buf[4096];

   vsnprintf(buf, 4096, text, args);

   int len = strlen(buf);

   for (int i = 0; i < len; i++) {

      if (buf[i] < 32) {
         if (buf[i] == '\n') {
            this->x = 0;
            this->y++;
            if (this->y >= this->bufferh) {
               this->y = 0;
            }
         }
         continue;
      }

      this->buffer[this->y * this->bufferw + this->x] = buf[i];
      this->x++;

      if (this->x >= this->bufferw) {
         this->x = 0;
         this->y++;
         if (this->y >= this->bufferh) {
            this->y = 0;
         }
      }

   }

   this->bufferChanged = true;

}

#endif // ARDUINO
