/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef Terminal_hpp
#define Terminal_hpp

#ifndef ARDUINO

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <signal.h>
#include <unistd.h>
#include <termios.h>
#include <sys/ioctl.h>

#include "../ServiceAbstract/ServiceAbstract.hpp"

class Terminal: public ServiceAbstract {

   public:
      Terminal(SysContext* systemContext);

      // AstroConfigurable: returns a service name
      const char* getName() { return (char*)"Terminal"; };

      // Configures a single configurable property (only enabled can be configured for the terminal)
      virtual bool configure(char* key, const void* value);

      // Does not apply for Terminal, but must be implemented
      virtual bool reconfigure();

      // Service: initializes the terminal
      bool initialize();

      // Service: initializes the terminal
      void finalize();

      // Service: Renders a dirty backbuffer and clears it for further usage
      void update();


      // Disables otput of characters when user presses a key
      void inputEchoOff();

      // Resets input to its original state
      void inputReset();


      // Starts the full screen, back buffered mode
      void startAlternate();

      // Exits the full screen mode
      void doneAlternate();


      // Instructs the terminal to hide the cursor
      void hideCursor();

      // Instruct the terminal to show the cursor
      void showCursor();


      // Clears the terminal or back buffer in full screen mode and moves cursor to 0, 0
      void clear();

      // Moves a cursor to given position on screen or in buffer in full screen mode
      void moveTo(int x, int y);

      // Prints the text to the current cursor position
      void print(const char *text, ...);
      void print(const char *text, va_list args);

      // Prints the text to the requested cursor position
      void printAt(int x, int y, const char *text, ...);
      void printAt(int x, int y, const char *text, va_list args);

      // flusesh the stdio output
      void flush();

   private:

      // hold pointer to original terminal state stored in termios structure
      struct termios* termiosOrig;

      // flag indicating the cursor was hidden
      bool cursorHidden;

      // flag indicating the window resizing event occured and resize was not performed yet
      bool resizing;

      // indicates the fullscreen mode is enabled
      bool buffered;

      // widtch of the back buffer
      int bufferw;

      // height of the back buffer
      int bufferh;

      // pointer to currently allocated character back buffer
      char* buffer;

      // indicated the back buffer was modified
      bool bufferChanged;

      // holds current cursor position in the backbuffer
      int x;
      int y;


      // Holds reference of the current terminal to be possoble to pass the SIGWINCH event
      static class Terminal* terminal;

      // SIGWINCH signal handler (static function linkable to signal syscall)
      static void onSIGWINCH(int sig) {
         if (sig != SIGWINCH) return;
         Terminal::terminal->onResize();
      }

      // SIGWINCH signal handler (Terminal object)
      void onResize();

      // Resizes the window
      void resize();

      // Prints the text to the back buffer
      void printout(const char *text, va_list args);

};

#endif // ARDUINO

#endif
