/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef ARDUINO

#include "InfoPrint.hpp"

InfoPrint::InfoPrint(SysContext* systemContext, Terminal* terminal): ServiceAbstract(systemContext) {
   this->terminal = terminal;
}

/* ------------------------------------------------------------ */
/* Public                                                       */
/* ------------------------------------------------------------ */

bool InfoPrint::configure(char* key, const void* value) {

   if (strcmp(key, (char*)"enabled") == 0) {
      this->shadowConfig.enabled = *(bool*)value;
      return true;
   }

   return false;

}


bool InfoPrint::reconfigure() {

   if (this->shadowConfig.enabled != this->enabled) {
      this->enabled = this->shadowConfig.enabled;

      /*if (this->enabled) {
         this->initialize();
      } else {
         this->finalize();
      }*/

   }

   return true;

}

bool InfoPrint::initialize() {

   if (!this->enabled) {
      this->state = (char*)"Service is disabled";
      return true;
   }

   if (this->initialized) return true;

   #ifdef DIAG
   this->systemContext->diag->debug((char*)"InfoPrint::initialize", (char*)"Initializing info print service");
   this->systemContext->diag->debug((char*)"InfoPrint::initialize", (char*)"Switching to alternate screen buffer will hide some log records logged to screen!");
   #endif

   this->terminal->startAlternate();
   this->terminal->hideCursor();
   this->terminal->clear();

   this->initialized = true;
   this->state =(char*)"Ready";

   return true;

}

void InfoPrint::finalize() {

   #ifdef DIAG
   this->systemContext->diag->debug((char*)"InfoPrint::finalize", (char*)"Finalizing info print service");
   #endif

   this->initialized = false;

   this->terminal->showCursor();
   this->terminal->doneAlternate();

}

void InfoPrint::update() {

   if (!this->initialized) return;
   this->printInfo();

}

/* ------------------------------------------------------------ */
/* Protected                                                    */
/* ------------------------------------------------------------ */

void InfoPrint::printInfo() {
   this->terminal->printAt(0, 0, "Infoprinter!\n");
}

/* ------------------------------------------------------------ */
/* Private                                                      */
/* ------------------------------------------------------------ */

// --------------- Device information printers ----------------

void InfoPrint::atmosphereHumidity(HumiditySensor* device){
}

void InfoPrint::atmospherePressure(PressureSensor* device) {
}

void InfoPrint::atmosphereTemperature(TemperatureSensor* device) {
}

void InfoPrint::controlJoystick(Joystick* device) {

   printf("Joystick state:     %s\n", device->getState());
   printf("Joystick buttons:   %d\n", device->getButtonCount());
   printf("Joystick axes:      %d\n\n", device->getAxisCount());

   int max = device->getButtonCount();
   if (device->getAxisCount() > max) max = device->getAxisCount();

   for (int i = 0; i < max; i++) {

      if (i < device->getButtonCount()) {
         char* state = device->getButtonState(i) ? (char*) "On " : (char*) "Off";
         printf("Joystick button %02d: %s          ", i + 1, state);
      }

      if (i < device->getAxisCount()) {
         struct JoystickAxisState state;
         device->getAxisState(i, &state);
         printf("Joystick axis %02d:   x = %6d, y = %6d", i + 1, state.x, state.y);
      }

      printf("\n");
   }

}

void InfoPrint::localizationSensor(LocalizationSensor* device) {

   struct LocalizationData localizationData;
   struct tm ts;
   char buf[80];

   device->getLocalizationData(&localizationData);

   time_t time = localizationData.time;
   ts = *localtime(&time);
   strftime(buf, sizeof(buf), "%a %Y-%m-%d %H:%M:%S %Z", &ts);

   printf("Localization (latitude):  %f\n",  localizationData.latitude);
   printf("Localization (longitude): %f\n",  localizationData.longitude);
   printf("Localization (altitude):  %f\n",  localizationData.altitude);
   printf("Localization (time):      %lf\n", localizationData.time);
   printf("Localization (time):      %s\n",  buf);
   printf("Localization (status):    %s\n",  device->getState());

}

void InfoPrint::motionTrackingAccelerometer3D(Accelerometer3D* device) {
}

void InfoPrint::motionTrackingGyroscope3D(Gyroscope3D* device) {
}

void InfoPrint::motionTrackingMagnetometer3D(Magnetometer3D* device) {
}

void InfoPrint::stepperMotorPositionMotionController(PositionMotionController* device) {
}

// --------------- Service information printers ----------------

#endif // ARDUINO