/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef InfoPrint_hpp
#define InfoPrint_hpp

#ifndef ARDUINO

#include <time.h>

#include "../ServiceAbstract/ServiceAbstract.hpp"
#include "../Terminal/Terminal.hpp"

#include "../../hardware/hal/Hal.hpp"
#include "../../hardware/hal/Atmosphere/HumiditySensor.hpp"
#include "../../hardware/hal/Atmosphere/PressureSensor.hpp"
#include "../../hardware/hal/Atmosphere/TemperatureSensor.hpp"
#include "../../hardware/hal/Control/Joystick.hpp"
#include "../../hardware/hal/Localization/LocalizationSensor.hpp"
#include "../../hardware/hal/MotionTracking/Accelerometer3D.hpp"
#include "../../hardware/hal/MotionTracking/Gyroscope3D.hpp"
#include "../../hardware/hal/MotionTracking/Magnetometer3D.hpp"
#include "../../hardware/hal/StepperMotor/PositionMotionController.hpp"

struct InfoPrintConfig {
   bool enabled;
};

class InfoPrint: public ServiceAbstract {

   public:
      InfoPrint(SysContext* systemContext, Terminal* terminal);

      const char* getName() { return (char*)"InfoPrint"; }

      // Reconfigures a single configurable property
      virtual bool configure(char* key, const void* value);

      // Does not apply for Terminal, but must be implemented
      virtual bool reconfigure();

      virtual bool initialize();
      virtual void finalize();
      virtual void update();

   protected:
      void printInfo();

   private:
      class Terminal* terminal;

      struct InfoPrintConfig shadowConfig = {
         .enabled = false,
      };

      /* ------------------------------------------------ */
      /* Device information printers                      */
      /* ------------------------------------------------ */

      void atmosphereHumidity(HumiditySensor* device);
      void atmospherePressure(PressureSensor* device);
      void atmosphereTemperature(TemperatureSensor* device);

      void controlJoystick(Joystick* device);

      void localizationSensor(LocalizationSensor* device);

      void motionTrackingAccelerometer3D(Accelerometer3D* device);
      void motionTrackingGyroscope3D(Gyroscope3D* device);
      void motionTrackingMagnetometer3D(Magnetometer3D* device);

      void stepperMotorPositionMotionController(PositionMotionController* device);

      /* ------------------------------------------------ */
      /* Service information printers                     */
      /* ------------------------------------------------ */


};

#endif // ARDUINO

#endif