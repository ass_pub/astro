/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef SpiDev_hpp
#define SpiDev_hpp

#ifndef ARDUINO

#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/spi/spidev.h>
#include <unistd.h>

#include "../Spi.hpp"

struct SpiDevConfig {

   // spi device
   char *device;

   // number of bits per word
   unsigned char bitsPerWord;

    // SPI_MODE_0 (0,0) 	CPOL = 0, CPHA = 0, Clock idle low, data is clocked in on rising edge, output data (change) on falling edge
    // SPI_MODE_1 (0,1) 	CPOL = 0, CPHA = 1, Clock idle low, data is clocked in on falling edge, output data (change) on rising edge
    // SPI_MODE_2 (1,0) 	CPOL = 1, CPHA = 0, Clock idle high, data is clocked in on falling edge, output data (change) on rising edge
    // SPI_MODE_3 (1,1) 	CPOL = 1, CPHA = 1, Clock idle high, data is clocked in on rising, edge output data (change) on falling edge
   unsigned char mode;

   // Bus speed clock frequency
   unsigned int speed;
};

class SpiDev: public Spi {

   public:

      SpiDev(SpiDevConfig* config);
      ~SpiDev();

      bool openPort();
      void closePort();

      bool configurePort();
      bool configurePort(SpiDevConfig* config);

      bool writeRead(unsigned char* txData, unsigned char* rxData, int length);

   private:

      int spi_fd;

      SpiDevConfig* config;

};

#endif // ARDUINO

#endif