/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef ARDUINO

#include "SpiDev.hpp"

#include <stdio.h>

/* ------------------------------------------------------------ */
/* Public                                                       */
/* ------------------------------------------------------------ */

SpiDev::SpiDev(SpiDevConfig *config) {
   this->config = config;
}

SpiDev::~SpiDev() {
   closePort();
}

bool SpiDev::openPort () {

   this->spi_fd = open(this->config->device, O_RDWR);

   if (this->spi_fd < 0) {
      printf("Could not open SPI device");
      return false;
   }

   return this->configurePort();
}

void SpiDev::closePort()
{
   close(this->spi_fd);
   this->spi_fd = -1;
}

bool SpiDev::configurePort() {

   if (this->spi_fd < 0) return false;

   if (ioctl(this->spi_fd, SPI_IOC_WR_MODE, &this->config->mode) < 0) {
      close(this->spi_fd);
      printf("Could not set SPIMode (WR)...ioctl fail");
      return false;
   }

   if (ioctl(this->spi_fd, SPI_IOC_RD_MODE, &this->config->mode) < 0) {
      close(this->spi_fd);
      printf("Could not set SPIMode (RD)...ioctl fail");
      return false;
   }

   if (ioctl(this->spi_fd, SPI_IOC_WR_BITS_PER_WORD, &this->config->bitsPerWord) < 0) {
      close(this->spi_fd);
      printf("Could not set SPI bitsPerWord (WR)...ioctl fail");
      return false;
   }

   if (ioctl(this->spi_fd, SPI_IOC_RD_BITS_PER_WORD, &this->config->bitsPerWord) < 0) {
      close(this->spi_fd);
      printf("Could not set SPI bitsPerWord (RD)...ioctl fail");
      return false;
   }

   if (ioctl(this->spi_fd, SPI_IOC_WR_MAX_SPEED_HZ, &this->config->speed) < 0) {
      close(this->spi_fd);
      printf("Could not set SPI speed (WR)...ioctl fail");
      return false;
   }

   if (ioctl(this->spi_fd, SPI_IOC_RD_MAX_SPEED_HZ, &this->config->speed) < 0)
   {
      close(this->spi_fd);
      printf("Could not set SPI speed (RD)...ioctl fail");
      return false;
   }

   return true;

}

bool SpiDev::configurePort(SpiDevConfig *config) {
   this->config = config;
   return this->configurePort();
}

bool SpiDev::writeRead(unsigned char* txData, unsigned char* rxData, int length) {

	struct spi_ioc_transfer spi;

	spi.tx_buf = (unsigned long)txData;		         //transmit from "data"
	spi.rx_buf = (unsigned long)rxData;		         //receive into "data"
	spi.len = length;
	spi.delay_usecs = 0;
	spi.speed_hz = this->config->speed;
	spi.bits_per_word = this->config->bitsPerWord;
	spi.cs_change = 0;						            //0 = Set CS high after a transfer, 1 = leave CS set low

	if(ioctl(this->spi_fd, SPI_IOC_MESSAGE(1), &spi) < 0) {
		printf("Error - Problem transmitting spi data..ioctl");
      return false;
	}

   return true;

}

#endif // ARDUINO
