/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef PositionMotionController_hpp
#define PositionMotionController_hpp

#include "../Hal.hpp"

class PositionMotionController: public Hal {

   public:
      virtual bool resetCurentPosition() = 0;

      virtual bool getCurrentPosition(long* position) = 0;
      virtual bool getCurrentVelocity(float* velocity) = 0;
      virtual bool getTargetPosition(long* position) = 0;

      virtual bool setTargetPosition() = 0;

      virtual bool setAcceleration(float acceleration) = 0;
      virtual bool setDecelleration(float decelleration) = 0;
      virtual bool setMaxVelocity(float velocity) = 0;

};

#endif