/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef Timer_hpp
#define Timer_hpp

#include "../Hal.hpp"

/**
 * Enumeration of Timer types
 **/
enum TimerType {
	Periodic,
	Oneshot
};

class Timer;

/** Interface to be implemented on the timer event reciever class */
class TimerEventReciever {

   public:
      virtual void onTimer(Timer* sender) = 0;

};

class Timer: public Hal {

   public:
      virtual void start(long nanosecs, TimerType type, TimerEventReciever* eventReciever) = 0;
      virtual void stop() = 0;

};

#endif
