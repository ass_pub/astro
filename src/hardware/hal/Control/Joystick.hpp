/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef Joystick_hpp
#define Joystick_hpp

#include "../Hal.hpp"

struct JoystickAxisState {
   int x;
   int y;
};

class Joystick: public Hal {

   public:
      virtual int getAxisCount() = 0;
      virtual int getButtonCount() = 0;
      virtual bool getAxisState(int axis, struct JoystickAxisState* axisState) = 0;
      virtual bool getButtonState(int button) = 0;

};

#endif
