/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef TMC5160SPI_hpp
#define TMC5160SPI_hpp

#include "../../bus/spi/Spi.hpp"
#include "../../bus/gpio/Gpio.hpp"
#include "../../hal/StepperMotor/PositionMotionController.hpp"

enum TMC5160MCMode {
   Position,
   Velocity,
   StepDir
};

struct TMC5160SPIConfig {
   class Spi* spi;
   class Gpio *gpio;
   enum TMC5160MCMode mode;
   unsigned char csPin;
};

class TMC5160SPI: public PositionMotionController {

   private:

      struct TMC5160SPIConfig* config;

   public:
      virtual const char* getHalClass() { return (char*)"StepperMotor"; }
      virtual const char* getHalIds() { return (char*)"PositionMotionController"; }
      const char* getDeviceName() { return "Trinamic TMC5160 Stepper motor driver"; }

      TMC5160SPI(TMC5160SPIConfig* config);

      bool initialize();
      void finalize();

      bool resetCurentPosition();

      bool getCurrentPosition(long* position);
      bool getCurrentVelocity(float* velocity);
      bool getTargetPosition(long* position);

      bool setTargetPosition();

      bool setAcceleration(float acceleration);
      bool setDecelleration(float decelleration);
      bool setMaxVelocity(float velocity);

};

#endif
