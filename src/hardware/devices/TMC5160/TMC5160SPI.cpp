/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#include <stdio.h>

#include "TMC5160SPI.hpp"

/* ------------------------------------------------------------ */
/* Public                                                       */
/* ------------------------------------------------------------ */

TMC5160SPI::TMC5160SPI(TMC5160SPIConfig* config) {
   this->config = config;
}

bool TMC5160SPI::initialize() {

   if (this->config->mode != Position) {
      printf("ERROR: TMC5160 driver support only positioning mode\n");
      return false;
   }

   return true;

}

void TMC5160SPI::finalize() {
}


bool TMC5160SPI::resetCurentPosition() {
   return false;
}

bool TMC5160SPI::getCurrentPosition(long* position) {
   return false;
}

bool TMC5160SPI::getCurrentVelocity(float* velocity) {
   return false;
}

bool TMC5160SPI::getTargetPosition(long* position) {
   return false;
}

bool TMC5160SPI::setTargetPosition() {
   return false;
}

bool TMC5160SPI::setAcceleration(float acceleration) {
   return false;
}

bool TMC5160SPI::setDecelleration(float decelleration) {
   return false;
}

bool TMC5160SPI::setMaxVelocity(float velocity) {
   return false;
}