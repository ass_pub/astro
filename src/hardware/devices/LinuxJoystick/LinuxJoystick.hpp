/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef LinuxJoystick_hpp
#define LinuxJoystick_hpp

#ifndef ARDUINO

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <linux/joystick.h>

#include "../../hal/Control/Joystick.hpp"

class LinuxJoystick: public Joystick {

   public:
      const char* getDeviceName() { return "Linux Joystick"; }
      const char* getHalClass() { return (char*)"Control"; }
      const char* getHalIds() { return (char*)"Joystick"; }

      LinuxJoystick(const char* dev);
      ~LinuxJoystick();

      bool initialize();
      void update();

      bool isReady();
      bool readEvent();
      char *getState();

      int getAxisCount();
      int getButtonCount();
      bool getAxisState(int axis, struct JoystickAxisState* axisState);
      bool getButtonState(int button);

   private:

      bool opendev();
      void finalize();

      const char* dev;
      int fd;

      char* state;

      size_t axisCount;
      size_t buttonCount;

      bool* buttons;
      struct JoystickAxisState* axes;

};

#endif // ARDUINO

#endif
