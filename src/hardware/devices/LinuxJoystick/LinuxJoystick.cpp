/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef ARDUINO

#include "LinuxJoystick.hpp"

/* ------------------------------------------------------------ */
/* Public                                                       */
/* ------------------------------------------------------------ */

LinuxJoystick::LinuxJoystick(const char* dev) {

   this->dev = dev;
   this->fd = -1;
   this->state = (char*)"Not initialized";
   this->axisCount = 0;
   this->buttonCount = 0;

}


LinuxJoystick::~LinuxJoystick() {

   this->finalize();

}


bool LinuxJoystick::initialize() {

   this->opendev();
   return true;

}


void LinuxJoystick::update() {

   this->readEvent();

}


bool LinuxJoystick::isReady() {

   return this->fd != -1;

}


bool LinuxJoystick::readEvent() {

   ssize_t bytes;
   struct js_event event;

   if (this->fd == -1) {
      this->opendev();
      return false;
   }

   bytes = read(this->fd, &event, sizeof(event));

   if (bytes == -1 && (errno != EAGAIN || access(this->dev, F_OK) == -1)) {
      this->finalize();
      return false;
   }

   if (bytes != sizeof(event)) return false;

   switch(event.type) {

      case JS_EVENT_BUTTON:
         this->buttons[event.number] = event.value;
         break;

      case JS_EVENT_AXIS:

         if (event.number % 2 == 0) {
            this->axes[event.number / 2].x = event.value;
         } else {
            this->axes[event.number / 2].y = event.value;
         }

         break;

      default:
            /* Ignore other events. */
            break;

   }

   return true;

}


char* LinuxJoystick::getState() {

   return this->state;
}


int LinuxJoystick::getAxisCount() {
   return this->axisCount;
}


int LinuxJoystick::getButtonCount() {
   return this->buttonCount;
}


bool LinuxJoystick::getAxisState(int axis, struct JoystickAxisState* axisState) {
   if (axis < 0 || axis >= (int)this->axisCount) return false;
   *axisState = this->axes[axis];
   return true;
}


bool LinuxJoystick::getButtonState(int button) {
   if (button < 0 || button >= (int)this->buttonCount) return false;
   return this->buttons[button];
}

/* ------------------------------------------------------------ */
/* Private                                                      */
/* ------------------------------------------------------------ */

bool LinuxJoystick::opendev() {

   __u8 ab;

   if (this->fd != -1) return false;

   if (access(this->dev, F_OK) == -1) {
      this->state = (char*)"Device not found";
      return false;
   }

   int fd = open(this->dev, O_RDONLY | O_NONBLOCK);
   if (fd == -1) return false;

   this->fd = fd;

   this->axisCount = ioctl(fd, JSIOCGAXES, &ab) == -1 ? 0 : ab;
   this->buttonCount = ioctl(fd, JSIOCGBUTTONS, &ab) == -1 ? 0 : ab;

   this->axes = (struct JoystickAxisState*)malloc(this->axisCount * sizeof(JoystickAxisState));
   this->buttons = (bool*)malloc(this->buttonCount * sizeof(bool));

   for (unsigned int i = 0; i < this->axisCount; i++) {
      this->axes[i].x = 0;
      this->axes[i].y = 0;
   }

   for (unsigned int i = 0; i < this->buttonCount; i++) {
      this->buttons[i] = false;
   }

   this->state = (char*)"Ready";

   return true;

}


void LinuxJoystick::finalize() {

   if (this->fd == -1) return;

   close(this->fd);

   this->fd = -1;
   this->axisCount = 0;
   this->buttonCount = 0;

   free(this->axes);
   free(this->buttons);

   this->state = (char*)"Not initialized";

}

#endif // ARDUINO