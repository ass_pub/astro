/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef ARDUINO

#include <stdio.h>
#include <math.h>
#include <errno.h>
#include <time.h>
#include <string.h>

#include "Gpsd.hpp"

/* ------------------------------------------------------------ */
/* Public                                                       */
/* ------------------------------------------------------------ */

Gpsd::Gpsd(GpsdConfig* config) {

   this->config = config;
   this->initialized = false;
   this->ready = false;
   this->state = (char*)"Constructed";
   this->connecting = false;

}

bool Gpsd::initialize() {

   this->state = (char*)"Initialized";
   this->connecting = true;
   this->reconnect();
   this->config->timer->start(1000000000, Periodic, this);

   return true;

}

void Gpsd::finalize() {
   this->config->timer->stop();
   gps_close(&this->gps_data);
}

bool Gpsd::getLocalizationData(LocalizationData* data) {
   memcpy(data, &this->localizationData, sizeof(LocalizationData));
   return true;
}

/* ------------------------------------------------------------ */
/* Private                                                      */
/* ------------------------------------------------------------ */

void Gpsd::onTimer(Timer* sender) {

   if (this->connecting) {
      this->reconnect();
      return;
   }

   int result = gps_read(&this->gps_data);

   if (result == -1) {
      this->state = (char*)"Read failed";
      this->reconnect();
      return;
   }

   if (result == 0) return;

   while (gps_read(&this->gps_data) > 0) {}

   if (!isnan(this->gps_data.fix.latitude)) this->localizationData.latitude = this->gps_data.fix.latitude;
   if (!isnan(this->gps_data.fix.longitude)) this->localizationData.longitude = this->gps_data.fix.longitude;
   if (!isnan(this->gps_data.fix.altitude)) this->localizationData.altitude = this->gps_data.fix.altitude;
   if (!isnan(this->gps_data.fix.time)) this->localizationData.time = this->gps_data.fix.time;

}

void Gpsd::reconnect() {

   if (!this->connecting) gps_close(&gps_data);

   this->connecting = (char*)"Connecting to host";

   if ((gps_open(this->config->hostname, this->config->port, &this->gps_data)) == -1) {
      gps_close(&this->gps_data);
      return;
   }

   gps_stream(&gps_data, WATCH_ENABLE | WATCH_JSON, nullptr);

   this->connecting = false;
   this->state = (char*)"Connected";

}

#endif // ARDUINO

