/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef Gpsd_hpp
#define Gpsd_hpp

#ifndef ARDUINO

#include <gps.h>

#include "../../hal/Timer/Timer.hpp"
#include "../../hal/Localization/LocalizationSensor.hpp"

struct GpsdConfig {
   class Timer *timer;
   char *hostname;
   char *port;
};

class Gpsd: public LocalizationSensor, TimerEventReciever {

   public:
      const char* getDeviceName() { return "GPSD daemon TCP Client"; }
      const char* getHalClass() { return (char*)"Localization"; }
      const char* getHalIds() { return (char*)"LocalizationSensor"; }

      Gpsd(GpsdConfig* gpsdConfig);

      bool initialize();
      void finalize();

      bool getLocalizationData(LocalizationData* data);

   private:

      bool connecting;

      struct GpsdConfig* config;
      struct gps_data_t gps_data;
      struct LocalizationData localizationData;

      void onTimer(Timer* sender);
      void reconnect();

};

#endif // ARDUINO

#endif
