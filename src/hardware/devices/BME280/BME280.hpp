/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef BME280_hpp
#define BME280_hpp

#include "../../hal/Atmosphere/TemperatureSensor.hpp"
#include "../../hal/Atmosphere/HumiditySensor.hpp"
#include "../../hal/Atmosphere/PressureSensor.hpp"

class BME280: TemperatureSensor, HumiditySensor, PressureSensor {

   public:

      const char* getDeviceName() { return "Bosch BME280 Temperature, humidity, pressure sensor"; }
      const char* getHalIds() { return "TempertureSensor,HumiditySensor,PresureSensor"; }

      bool getTemperature(float* temperature);
      bool setSeaLevelPressure(float* pressure);
      bool getPressure(float* pressure);
      bool getHumidity(float* humidity);

};

#endif