/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef ARDUINO

#include "LinuxTimer.hpp"

LinuxTimer::LinuxTimer() {
   this->initialized = false;
   this->ready = false;
   this->state = (char*)"Constructed";

   this->eventReciever = nullptr;
   this->running = false;
}


bool LinuxTimer::initialize() {

   this->state = (char*)"Initializing";

   if (this->initialized) return false;

	this->sa.sa_flags = SA_SIGINFO;
	this->sa.sa_sigaction = this->handler;
	sigemptyset(&this->sa.sa_mask);
   sigaddset(&this->sa.sa_mask, SIGUSR1);

	if (sigaction(SIGUSR1, &this->sa, nullptr) == -1) {
      this->state = (char*)"Failed to configure timer sigaction";
      this->timerid = 0;
      return false;
   }

   this->sev.sigev_notify = SIGEV_SIGNAL;
   this->sev.sigev_signo = SIGUSR1;
   this->sev.sigev_value.sival_ptr = this;

	if (timer_create(CLOCK_MONOTONIC, &this->sev, &this->timerid) == -1) {
      this->state = (char*)"Failed to create a timer";
      this->timerid = 0;
      return false;
   }

   this->initialized = true;
   this->ready = true;
   this->state = (char*)"Stopped";

   return true;

}


void LinuxTimer::finalize() {
   this->stop();
}


void LinuxTimer::start(long nanosecs, TimerType type, TimerEventReciever* eventReciever) {
   if (this->running) return;
   this->eventReciever = eventReciever;
   this->running = this->_start(nanosecs, type);
};


void LinuxTimer::stop() {
   if (!this->running) return;
   this->_stop();
   this->running = false;
   this->eventReciever = nullptr;
}

void LinuxTimer::timerEvent() {
   if (this->eventReciever != nullptr) this->eventReciever->onTimer(this);
}

bool LinuxTimer::_start(long nanosecs, TimerType type)
{
   struct itimerspec its;

   if (this->timerid <= 0) return false;

	switch(type){

		case(Periodic):
			its.it_value.tv_sec = nanosecs / 1000000000;
			its.it_value.tv_nsec = nanosecs % 1000000000;
			its.it_interval.tv_sec = nanosecs / 1000000000;
			its.it_interval.tv_nsec = nanosecs % 1000000000;
			break;

		case(Oneshot):
			its.it_value.tv_sec = nanosecs / 1000000000;
			its.it_value.tv_nsec = nanosecs % 1000000000;
			its.it_interval.tv_sec = 0;
			its.it_interval.tv_nsec = 0;
			break;
	}

   if (timer_settime(timerid, 0, &its, nullptr) != -1) {
      this->state = (char*)"Running";
   	return true;
   } else {
      this->state = (char*)"Failed to run";
   	return true;
   }

}


void LinuxTimer::_stop() {

   if (this->timerid <= 0) return;

	struct itimerspec itsnew;
	itsnew.it_value.tv_sec = 0;
	itsnew.it_value.tv_nsec = 0;
	itsnew.it_interval.tv_sec = 0;
	itsnew.it_interval.tv_nsec = 0;

	timer_settime(timerid, 0, &itsnew, nullptr);

   this->state = (char*)"Stopped";

}


LinuxTimer::~LinuxTimer() {

   this->_stop();
	signal(SIGRTMIN, SIG_IGN);
	timer_delete(timerid);

}

#endif // ARDUINO