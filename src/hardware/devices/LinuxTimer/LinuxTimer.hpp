/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef LinuxTimer_hpp
#define LinuxTimer_hpp

#ifndef ARDUINO

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <time.h>

#include "../../hal/Timer/Timer.hpp"

class LinuxTimer: public Timer {

   public:
      LinuxTimer();
      virtual ~LinuxTimer();

      virtual bool initialize();
      virtual void finalize();

      virtual void start(long nanosecs, TimerType type, TimerEventReciever* eventReciever);
      virtual void stop();

   protected:
      const char* getDeviceName() { return (char*)"LinuxTimer"; }
      const char* getHalClass() { return (char*)"Timer"; }
      const char* getHalIds() { return (char*)"Timer"; }

      void timerEvent();

      bool _start(long nanosecs, TimerType type);
      void _stop();

   private:
      bool running;
      class TimerEventReciever* eventReciever;

      timer_t timerid;
      struct sigevent sev;
      struct sigaction sa;

      static void handler(int sig, siginfo_t *si, void *uc ) {
         (reinterpret_cast<LinuxTimer*>(si->si_value.sival_ptr))->timerEvent();
      }

};

#endif // ARDUINO

#endif
