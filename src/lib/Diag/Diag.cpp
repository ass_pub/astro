/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#include "Diag.hpp"

Diag::Diag(Logger** loggers) {
   this->loggers = loggers;
   this->__logLevel =  INFO;
}

void Diag::setLogLevel(LogLevel logLevel) {
   this->__logLevel = logLevel;
}

class Logger** Diag::getLoggers() {
   return this->loggers;
}

void Diag::debug(char* component, char* text, ...) {

   if (this->__logLevel > 0) return;

   va_list args;
   va_start(args, text);
   this->log((char*)"DEBUG", component, text, args);
   va_end(args);

}

void Diag::info(char* component, char* text, ...) {

   if (this->__logLevel > 1) return;

   va_list args;
   va_start(args, text);
   this->log((char*)"INFO ", component, text, args);
   va_end(args);

}

void Diag::warning(char* component, char* text, ...) {

   if (this->__logLevel > 2) return;

   va_list args;
   va_start(args, text);
   this->log((char*)"WARN ", component, text, args);
   va_end(args);

}

void Diag::error(char* component, char* text, ...) {

   va_list args;
   va_start(args, text);
   this->log((char*)"ERROR", component, text, args);
   va_end(args);

}

void Diag::log(char* severity, char *component, char* text, va_list args) {

   char lb1[40];
   char lb2[80];

   if (strlen(component) != 0 && strlen(text) != 0) {
      snprintf(lb1, sizeof(lb1), "[%s] %-25s ", severity, component);
      vsnprintf(lb2, sizeof(lb2), text, args);
   } else {
      lb1[0] = 0;
      lb2[0] = 0;
   }

   char* logbuf = (char*)malloc(strlen(lb1) + strlen(lb2) + 1);
   strcpy(logbuf, lb1);
   strcat(logbuf, lb2);

   int i = 0;
   while(this->loggers[i] != nullptr) {

      if (this->loggers[i]->getEnabled()) {
         this->loggers[i]->log(logbuf);
      }

      i++;

   }

   free(logbuf);

}
