/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef FileLogger_hpp
#define FileLogger_hpp

#ifndef ARDUINO

#include <stdio.h>
#include <string.h>

#include "../Logger.hpp"

struct FileLoggerConfig {
   bool enabled;
   char* filename;
};

class FileLogger: public Logger {

   public:
      FileLogger();

      virtual const char* getName() { return (char*)"FileLogger"; }
      virtual bool getEnabled();

      virtual bool configure(char* key, const void* value);
      virtual bool reconfigure();

      virtual void log(char* text);

   private:
      bool enabled;

      struct FileLoggerConfig shadowConfig = {
         .enabled = false,
         .filename = (char*)"stdout"
      };

      char* filename = (char*)"stdout";
      FILE* fd = nullptr;

      bool open();
      void close();

};

#endif

#endif