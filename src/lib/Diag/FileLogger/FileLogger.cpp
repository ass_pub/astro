/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef ARDUINO

#include "FileLogger.hpp"

FileLogger::FileLogger() {
   this->enabled = false;
}

bool FileLogger::getEnabled() {
   return this->enabled;
}


bool FileLogger::configure(char* key, const void* value) {

   if (strcmp(key, (char*)"enabled") == 0) {
      this->shadowConfig.enabled = *(bool*)value;
      return true;
   }

   if (strcmp(key, (char*)"filename") == 0) {
      this->shadowConfig.filename = (char*)value;
      return true;
   }

   return false;

}


bool FileLogger::reconfigure() {

   bool configChanged = false;

   if (this->shadowConfig.enabled != this->enabled) {
      this->enabled = this->shadowConfig.enabled;
      configChanged = true;
   }

   if (strcmp(this->filename, this->shadowConfig.filename) != 0) {
      this->filename = this->shadowConfig.filename;
      configChanged = true;
   }

   if (configChanged) {

      if (this->fd != nullptr) {
         this->close();
      }

      if (this->enabled) {
         if (!this->open()) {
            this->enabled = false;
            return false;
         }
      }

   }

   return true;

}


bool FileLogger::open() {

   if (this->fd != nullptr) this->close();

   if (strcmp(this->filename, (char*)"stdout") == 0) {
      this->fd = stdout;
      return true;
   }

   if (strcmp(this->filename, (char*)"stderr") == 0) {
      this->fd = stderr;
      return true;
   }

   this->fd = fopen(this->filename, "a+");
   return this->fd != nullptr;

}


void FileLogger::close() {

   if (this->fd != stdout && this->fd != stderr) {
      fclose(this->fd);
   }

   this->fd = nullptr;

}


void FileLogger::log(char* text) {

   if (this->fd == nullptr) return;
   fprintf(this->fd, "%s\n", text);
   fflush(this->fd);

}

#endif