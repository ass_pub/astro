/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef ARDUINO

#include "JournalLogger.hpp"

JournalLogger::JournalLogger() {
   this->enabled = false;
}

bool JournalLogger::getEnabled() {
   return this->enabled;
}


bool JournalLogger::configure(char* key, const void* value) {

   if (strcmp(key, (char*)"enabled") == 0) {
      this->shadowConfig.enabled = *(bool*)value;
      return true;
   }

   return false;

}


bool JournalLogger::reconfigure() {

   if (this->shadowConfig.enabled != this->enabled) {
      this->enabled = this->shadowConfig.enabled;
   }

   return true;

}

void JournalLogger::log(char* text) {

   char chprio[8];
   strncpy(chprio, text, 7);
   chprio[7] = 0;

   int prio;

   // LOG_EMERG, LOG_ALERT, LOG_CRIT, LOG_ERR, LOG_WARNING, LOG_NOTICE, LOG_INFO, LOG_DEBUG
   if (strcmp(chprio, (char*)"[DEBUG]")) prio = LOG_DEBUG; else
   if (strcmp(chprio, (char*)"[INFO ]")) prio = LOG_INFO; else
   if (strcmp(chprio, (char*)"[WARN ]")) prio = LOG_WARNING; else
   if (strcmp(chprio, (char*)"[ERROR]")) prio = LOG_ERR;

   sd_journal_print(prio, text);
}

#endif