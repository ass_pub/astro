/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef Diag_hpp
#define Diag_hpp

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "Logger.hpp"

enum LogLevel {
   DEBUG = 0,
   INFO,
   WARNING,
   ERROR
};

class Diag {

   public:
      Diag(Logger** loggers);

      void setLogLevel(LogLevel logLevel);
      class Logger** getLoggers();

      void debug(char* component, char* text, ...);
      void info(char* component, char* text, ...);
      void warning(char* component, char* text, ...);
      void error(char* component, char* text, ...);

   private:
      class Logger** loggers;
      enum LogLevel __logLevel;

      void log(char* severity, char *component, char* text, va_list args);

};

#endif