/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#include "AstroConfig.hpp"


AstroConfig::AstroConfig(SysContext* systemContext, char** fileNames) {
   this->systemContext = systemContext;
   this->fileNames = fileNames;
   this->fileBuffer = nullptr;
   this->statusBuffer = (char*)malloc(80);
   this->status = (char*)"Ready";
}


AstroConfig::~AstroConfig() {

   free(this->statusBuffer);

   if (this->fileBuffer != nullptr) {
      free(this->fileBuffer);
      this->fileBuffer = nullptr;
   }

   if (this->jsonObj != nullptr) {
      free(this->jsonObj);
      this->jsonObj = nullptr;
   }

}


char* AstroConfig::getStatus() {
   return this->status;
}


bool AstroConfig::load() {

   if (this->fileBuffer != nullptr) free(this->fileBuffer);

   int i = 0;
   while (this->fileNames[i] != nullptr) {
      if (this->__loadFile(this->fileNames[i])) {
         this->status = (char*)"Ready";
         return true;
      }
      i++;
   }

   this->status = (char*)"Failed to load one of specified configuration files";

   return false;
}


bool AstroConfig::parse() {

   if (this->fileBuffer == nullptr) {
      this->status = (char*)"Json buffer empty";
      return false;
   }

   bool result = this->__parse(this->fileBuffer);

   free(this->fileBuffer);
   this->fileBuffer = nullptr;

   if (!result) return false;

   this->status = (char*)"Ready";
   return true;
}


bool AstroConfig::parse(char* json) {

   if (json == nullptr) {
      this->status = (char*)"Json buffer empty";
      return false;
   }

   if (!this->__parse(json)) return false;

   this->status = (char*)"Ready";
   return true;

}


bool AstroConfig::configure() {
   return this->__configure();
}


bool AstroConfig::__loadFile(char* filename) {

   // check if file exists
   if (access(filename, F_OK) == -1) return false;

   FILE *f = fopen(filename, "rb");
   if (f == nullptr) return false;

   // get file size
   fseek(f, 0, SEEK_END);
   long fsize = ftell(f);
   fseek(f, 0, SEEK_SET);

   if (fsize == 0) {
      fclose(f);
      return false;
   }

   // allocate memory for the file
   this->fileBuffer = (char*)malloc(fsize);
   if (this->fileBuffer == nullptr) {
      fclose(f);
      return false;
   }

   // read file to memory
   long rbytes = fread(this->fileBuffer, 1, fsize, f);
   if (rbytes != fsize) {
      free(this->fileBuffer);
      this->fileBuffer = nullptr;
      fclose(f);
      return false;
   }

   fclose(f);

   return true;
}


bool AstroConfig::__parse(char* json) {

   enum json_tokener_error error;

   if (this->jsonObj != nullptr) {
      free(this->jsonObj);
      this->jsonObj = nullptr;
   }

   this->jsonObj = json_tokener_parse_verbose(json, &error);

   if (this->jsonObj == nullptr) {
      this->status = (char*)json_tokener_error_desc(error);
      return false;
   }

   return true;

}


bool AstroConfig::__configure() {

   json_object_object_foreach(this->jsonObj, key, val) {

      enum json_type type = json_object_get_type(val);

      // Check for config section and eventually confiugre diag (and loggers)
      if (strcmp(key, (char*)"diag") == 0 && type == json_type_object) {
         if (!this->__configureDiag(val)) return false;
      } else

      // Check for config section and eventually confiugre drivers
      if (strcmp(key, (char*)"drivers") == 0 && type == json_type_object) {
         if (!this->__configureDriversServices(val, (AstroConfigurable**)this->systemContext->drivers)) return false;
      } else

      // Check for config section and eventually confiugre services
      if (strcmp(key, (char*)"services") == 0 && type == json_type_object) {
         if (!this->__configureDriversServices(val, (AstroConfigurable**)this->systemContext->services)) return false;
      } else

      // Invalid config file section
      {
         snprintf(this->statusBuffer, 80, (char*)"Invalid configuration option '%s'", key);
         this->status = this->statusBuffer;
         return false;
      }

   }

   return true;

}

bool AstroConfig::__configureDiag(json_object* jobj) {

   // walk the top level json object
   json_object_object_foreach(jobj, key, val) {

      enum json_type type = json_object_get_type(val);

      // Check for configuration option and confiugre log level
      if (strcmp(key, (char*)"logLevel") == 0 && type == json_type_string) {

         const char* v = json_object_get_string(val);
         if (!this->__configureDiagLogLevel(v)) return false;

      } else

      // Logger configuration must be an object
      if (type == json_type_object) {

         char loggerName[21];
         int namedLoggerIndex = -1;

         // get logger name and index

         char* dotPosCh = strchr(key, '.');
         int dotPos = dotPosCh == nullptr ? -1 : (int)(dotPosCh - key);

         // if . is specified, get index, otherwise index is -1

         if (dotPos >= 0) {
            strncpy(loggerName, key, dotPos);
            loggerName[dotPos] = 0;
            namedLoggerIndex = strtol(&key[dotPos + 1], nullptr, 10);
         } else {
            strncpy(loggerName, key, 20);
         }

         // check if specified logger and index exists
         if (!this->__checkConfigurable(
               (char*)"logger",
               (AstroConfigurable**)this->systemContext->diag->getLoggers(),
               loggerName,
               namedLoggerIndex
            )) return false;

         // configure logger
         if (!this->__configureConfigurable(
               (char*)"logger",
               (AstroConfigurable**)this->systemContext->diag->getLoggers(),
               loggerName,
               namedLoggerIndex,
               val
            )) return false;

      } else

      // Invalid option or value type
      {
         snprintf(this->statusBuffer, 80, (char*)"Invalid configuration option '%s' (json object expected)", key);
         this->status = this->statusBuffer;
         return false;
      }

   }

   return true;

}


bool AstroConfig::__configureDiagLogLevel(const char* logLevel) {

   if (strcmp(logLevel, (char*)"DEBUG") == 0) {
      this->systemContext->diag->setLogLevel(DEBUG);
      return true;
   } else

   if (strcmp(logLevel, (char*)"INFO") == 0) {
      this->systemContext->diag->setLogLevel(INFO);
      return true;
   } else

   if (strcmp(logLevel, (char*)"WARNING") == 0) {
      this->systemContext->diag->setLogLevel(WARNING);
      return true;
   } else

   if (strcmp(logLevel, (char*)"ERROR") == 0) {
      this->systemContext->diag->setLogLevel(ERROR);
      return true;
   } else

   {
      snprintf(this->statusBuffer, 80, (char*)"Invalid 'diag.logLevel' value '%s'", logLevel);
      this->status = this->statusBuffer;
      return false;
   }

}

bool AstroConfig::__configureDriversServices(json_object* jobj, AstroConfigurable** configurables) {

   json_object_object_foreach(jobj, key, val) {

      enum json_type type = json_object_get_type(val);

      // configuration section must be an object
      if (type == json_type_object) {

         char configurableName[21];
         int namedConfigurableIndex = -1;

         // get configurable name and index
         char* dotPosCh = strchr(key, '.');
         int dotPos = dotPosCh == nullptr ? -1 : (int)(dotPosCh - key);

         // if . is specified, get index, otherwise index is -1
         if (dotPos >= 0) {
            strncpy(configurableName, key, dotPos);
            configurableName[dotPos] = 0;
            namedConfigurableIndex = strtol(&key[dotPos + 1], nullptr, 10);
         } else {
            strncpy(configurableName, key, 20);
         }

         // check if specified configurable and index exists
         if (!this->__checkConfigurable(
               (char*)"logger",
               configurables,
               configurableName,
               namedConfigurableIndex
            )) return false;

         // configure
         if (!this->__configureConfigurable(
               (char*)"logger",
               configurables,
               configurableName,
               namedConfigurableIndex,
               val
            )) return false;

      }

   }

   return true;

}


bool AstroConfig::__checkConfigurable(
   char* configurableGroupName,
   AstroConfigurable** configurables,
   char* configurableName,
   int namedConfigurableIndex
) {

   // count named configurables
   int count = 0;
   int i = 0;
   while (configurables[i] != nullptr) {
      if (strcmp(configurables[i]->getName(), configurableName) == 0) count++;
      i++;
   }

   // check if named configurables exist at all
   if (count == 0) {
      snprintf(this->statusBuffer, 80, "Invalid %s name '%s'", configurableGroupName, configurableName);
      this->status = this->statusBuffer;
      return false;
   }

   // check if specified configurable is within the specified indec
   if ((namedConfigurableIndex < 0 && count > 1) || (namedConfigurableIndex >= 0 && namedConfigurableIndex >= count)) {
      snprintf(this->statusBuffer, 80, "Invalid named %s '%s' index", configurableGroupName, configurableName);
      this->status = this->statusBuffer;
      return false;
   }

   return true;

}


bool AstroConfig::__configureConfigurable(
   char* configurableGroupName,
   AstroConfigurable** configurables,
   char* configurableName,
   int namedConfigurableIndex,
   json_object* jobj
) {

   // find named configurable with given index
   class AstroConfigurable* configurable = this->__getConfigurable(configurables, configurableName, namedConfigurableIndex);

   // return with internal error message if not found
   if (configurable == nullptr) {
      snprintf(this->statusBuffer, 80, "Internal error: Unable to find %s (%s[%i])", configurableGroupName, configurableName, namedConfigurableIndex);
      this->status = this->statusBuffer;
      return false;
   }

   // configure configurable from json object
   json_object_object_foreach(jobj, key, val) {

      enum json_type type = json_object_get_type(val);

      bool bval;
      double dval;
      int ival;
      const void* value;

      switch (type) {

         case json_type_boolean: {
            bval = json_object_get_boolean(val);
            value = &bval;
            break;
         }

         case json_type_double: {
            dval = json_object_get_double(val);
            value = &dval;
            break;
         }

         case json_type_int: {
            ival = json_object_get_double(val);
            value = &ival;
            break;
         }

         case json_type_string: {
            value = json_object_get_string(val);
            break;
         }

         default:
            snprintf(this->statusBuffer, 80, "Invalid configuration option '%s.%s' value type", configurableName, key);
            this->status = this->statusBuffer;
            return false;
            break;

      }

      if (!configurable->configure(key, value)) {
         snprintf(this->statusBuffer, 80, "Invalid configuration option '%s.%s' or value out of bounds", configurableName, key);
         this->status = this->statusBuffer;
         return false;
      }

   }

   if (!configurable->reconfigure()) {
         snprintf(this->statusBuffer, 80, "Failed to reconfigure %s[%d]", configurableName, namedConfigurableIndex);
         this->status = this->statusBuffer;
         return false;
   }

   return true;
}

class AstroConfigurable* AstroConfig::__getConfigurable(AstroConfigurable** configurables,  char* configurableName, int namedConfigurableIndex) {

   class AstroConfigurable* configurable = nullptr;

   // normalize index
   int index = namedConfigurableIndex == -1 ? 0 : namedConfigurableIndex;

   // find named configurable on given index
   int i = 0;
   int ni = 0;

   while (configurables[i] != nullptr) {

      // check name and possibly named configurable index
      if (strcmp(configurables[i]->getName(), configurableName) == 0) {
         if (ni == index) {
            configurable = configurables[i];
            break;
         }
         ni++;
      }

      i++;
   }

   return configurable;

}
