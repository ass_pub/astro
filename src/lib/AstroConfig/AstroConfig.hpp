/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef AstroConfig_hpp
#define AstroConfig_hpp

#include <json-c/json.h>            // This is valid for debian package libjson-c only, other distros can have a different library name!
#include <unistd.h>

#include "AstroConfigurable.hpp"
#include "../../sys/SysContext.hpp"

class AstroConfig {

   public:
      AstroConfig(SysContext* systemContext, char** fileNames);
      ~AstroConfig();

      char* getStatus();

      bool load();
      bool parse();
      bool parse(char* json);
      bool configure();

   private:
      char* statusBuffer;
      char* status;
      class SysContext* systemContext;
      char** fileNames;
      char* fileBuffer;

      struct json_object* jsonObj;

      bool __loadFile(char* filename);
      bool __parse(char* json);
      bool __configure();

      bool __configureDiag(json_object* jobj);
      bool __configureDiagLogLevel(const char* logLevel);

      bool __configureDriversServices(json_object* jobj, AstroConfigurable** configurables);

      bool __checkConfigurable(char* configurableGroupName, AstroConfigurable** configurables,  char* configurableName, int namedConfigurableIndex);
      bool __configureConfigurable(char* configurableGroupName, AstroConfigurable** configurables,  char* configurableName, int namedConfigurableIndex, json_object* jobj);
      class AstroConfigurable* __getConfigurable(AstroConfigurable** configurables,  char* configurableName, int namedConfigurableIndex);

};

#endif