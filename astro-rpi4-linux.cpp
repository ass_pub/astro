/*----------------------------------------------------------------------*\
| Extensible multiplatform telescope mount controller                    |
| Copyright (C) 2020 Atom Software Studios                               |
|----------------------------------------------------------------------- |
| This program is free software: you can redistribute it and/or modify   |
| it under the terms of the GNU General Public License as published by   |
| the Free Software Foundation, either version 3 of the License, or      |
| (at your option) any later version.                                    |
|                                                                        |
| This program is distributed in the hope that it will be useful,        |
| but WITHOUT ANY WARRANTY; without even the implied warranty of         |
| MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          |
| GNU General Public License for more details.                           |
|                                                                        |
| You should have received a copy of the GNU General Public License      |
| along with this program.  If not, see <https://www.gnu.org/licenses/>. |
\*----------------------------------------------------------------------*/

#ifndef ARDUINO

#include <signal.h>
#include <stdarg.h>

#include "./src/version.hpp"
#include "./src/config/config.hpp"
#include "./src/lib/AstroConfig/AstroConfig.hpp"

/**
 * SIGINT (ctrl+c) signal handler
 */
void sigintHandler(int sig) {

   #ifdef DIAG

   char* signame;

   switch (sig) {
      case SIGINT:   signame = (char*)"SIGINT"; break;
      case SIGTERM:  signame = (char*)"SIGTERM"; break;
      case SIGKILL:  signame = (char*)"SIGKILL"; break;
      default:       signame = (char*)"Unknown"; break;
   }

   systemContext.diag->info((char*)"astro.sigintHandler", (char*)"Interrupt signal '%s' detected. Stopping", signame);

   #endif

   sys.stop();
}

/**
 * Parses command line arguments and sets appropriate variables
 */
void parseArgs(int argc, char *argv[], Options* options) {

   int opt;

   while ((opt = getopt(argc, argv, ":d")) != -1) {

      switch(opt) {
         case 'd':
            // appState.cmdLineArgs.argDebug = 1;
            break;
      }

   }

}

/**
 * Load & apply configuration file
 */
bool loadConfig() {

   char* fileNames[] = {
      (char*)"./astrocfg.jsonc",
      (char*)"/etc/astro/astrocfg.jsonc",
      (char*)nullptr
   };

   class AstroConfig* acfg = new AstroConfig(&systemContext, fileNames);

   if (!acfg->load()) {
      printf("Failed to load configuration: %s", acfg->getStatus());
      return false;
   }

   if (!acfg->parse()) {
      printf("Failed to parse configuration: %s\n", acfg->getStatus());
      return false;
   }

   if (!acfg->configure()) {
      printf("Invalid configuration: %s\n", acfg->getStatus());
      return false;
   }

   delete acfg;

   return true;

}

/**
 * Main entry point
 */
int main(int argc, char *argv[]) {

   // Build the system context
   systemContext.drivers = drivers;
   systemContext.services = services;

   #ifdef DIAG
   systemContext.diag = diagnostics;
   #endif

   parseArgs(argc, argv, systemContext.options);
   if (!loadConfig()) return 1;

   #ifdef DIAG
   systemContext.diag->info((char*)"astro.main", (char*)"--------------------------------------------------------------------------------------");
   systemContext.diag->info((char*)"astro.main", (char*)"%s %s", (char*)PRODUCT_NAME, (char*)PRODUCT_VERSION);
   systemContext.diag->info((char*)"astro.main", (char*)"%s", (char*)PRODUCT_COPYRIGHT);
   systemContext.diag->info((char*)"astro.main", (char*)"--------------------------------------------------------------------------------------");
   #endif

   // Trap interrupt signals (ctrl+c)
   signal(SIGINT, sigintHandler);
   signal(SIGTERM, sigintHandler);
   signal(SIGKILL, sigintHandler);

   // Start the system worker

   if (sys.initialize()) {
      sys.run();
      sys.finalize();
   }
   #ifdef DIAG
      else {
         systemContext.diag->debug((char*)"astro.main", (char*)"Building system context done");
      }
   #endif

   #ifdef DIAG
      systemContext.diag->info((char*)"astro.main", (char*)"Main loop terminated. Cleaning up");
   #endif

   // Release system worker
   signal(SIGINT, SIG_DFL);
   signal(SIGTERM, SIG_DFL);
   signal(SIGKILL, SIG_DFL);

   #ifdef DIAG
      systemContext.diag->info((char*)"astro.main", (char*)"Exiting");
      systemContext.diag->info((char*)"", (char*)"");
   #endif

   return 0;

}

#endif // ARDUINO